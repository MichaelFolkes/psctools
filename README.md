# PSCtools

An R package of tools that aid with generalized tasks specific to PSC.
To install in R:
    
```
install.packages("remotes") 
remotes::install_git("https://gitlab.com/MichaelFolkes/psctools")
```

Load `PSCtools` into memory:
```
require(PSCtools)
```

The function `writeScript` will make it easier to start things. The help file has an example:
```
?psctools::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "PSCtools")
```

To save and open a specific demo script (this one named "script_statweek"):
```
writeScript("script_statweek")

```
